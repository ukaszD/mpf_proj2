﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mpflab5
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Main(object sender, EventArgs e)
        {
            int startTemp = 820;
            DiffusionProcessController diffusionController1 = new DiffusionProcessController(chart1, chart1AInT, startTemp, "result_cost" + startTemp + ".txt");
            diffusionController1.prepateDataToDiffusionProcess();
            diffusionController1.startDiffusionProcess();
        }
    }
}
