﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace mpflab5
{
    class DiffusionProcessController
    {
        // 727 > T > 912    // T in Celcius
        double T;

        // 5 > ksi > tableRange
        int ksi = 1;

        const int iterationCount = 100000;
        const double timeStep = 1;
        const double dx = 1;

        //const data
        const double Q = 140000;
        const double R = 8.3144;
        const double d0 = 0.000041;
        const int tableRange = 100;

        double temp;    // T in Kelvin - initialize in constructor
        double D;

        List<double[]> c = new List<double[]>();
        List<double> integrals = new List<double>();
        System.IO.StreamWriter file;
        Chart chart;
        Chart chartAInT;
        double velocity;

        public DiffusionProcessController(Chart chart, Chart chartAInT, double startTemp, string resultFileName, double velocity = 0)
        {
            file = new System.IO.StreamWriter(resultFileName);
            this.T = startTemp;
            this.chart = chart;
            this.chartAInT = chartAInT;
            this.velocity = velocity;
        }

        double getTempInKelvin(double tempInCelcius)
        {
            return tempInCelcius + 273;
        }

        bool isNeumann()
        {
            return ((D * timeStep) / Math.Pow(dx, 2)) <= 0.5;
        }

        double[] fillTable(int ksi)
        {
            double[] initialRow = new double[tableRange];

            for (int i = 0; i <= ksi; i++)
            {
                initialRow[i] = 0.02;
            }

            for (int i = ksi + 1; i < tableRange; i++)
            {
                initialRow[i] = 0.1;
            }

            return initialRow;
        }

        double getCoefficientFromFeC(double t)
        {
            return -(t - 912) / 240.26;
        }

        double getProperlyObjectFromCTable(int currentIndex, int time)
        {
            if (currentIndex == this.ksi || currentIndex < this.ksi)
            {
                return getCoefficientFromFeC(this.T);
            }
            else if (currentIndex == tableRange)
            {
                return c[time][tableRange - 1];
            }
            else
            {
                return c[time][currentIndex];
            }
        }

        double countIntegralFromRow(double[] row)
        {
            double integral = 0;

            for (int i = this.ksi; i < row.Length; i++)
            {
                integral += (row[i] + row[i - 1]) * dx / 2;
            }

            return integral;
        }

        double[] countDiffusionInTimeStep(double D, int time)
        {
            double[] coefficientValuesInTimeStep = (double[])c[time].Clone();
            for (int j = this.ksi; j < tableRange; j++)
            {
                double tmpCoefficient = D * timeStep / Math.Pow(dx, 2);
                coefficientValuesInTimeStep[j] = (1 - 2 * tmpCoefficient) * getProperlyObjectFromCTable(j, time) + tmpCoefficient * (getProperlyObjectFromCTable(j - 1, time) + getProperlyObjectFromCTable(j + 1, time));
            }

            return coefficientValuesInTimeStep;
        }

        public void prepateDataToDiffusionProcess()
        {
            temp = getTempInKelvin(T);
            D = d0 * Math.Exp(-Q / (R * temp)) * 1E10;
            c.Add(fillTable(this.ksi));
        }

        void recountD()
        {
            temp = getTempInKelvin(T);
            D = d0 * Math.Exp(-Q / (R * temp)) * 1E10;
        }

        double lastTimeWhenTempChanged = 0;
        void changeTempUsingVelocity(double currentTimeStep, double velocity)
        {
            if (lastTimeWhenTempChanged != Math.Round(currentTimeStep, 2) && Math.Round(currentTimeStep, 2) % 1 == 0)
            {
                T += velocity;
                recountD();
                lastTimeWhenTempChanged = Math.Round(currentTimeStep, 2);
                Console.WriteLine(T);
            }
        }

        public void startDiffusionProcess()
        {
            double processFullTime = iterationCount * timeStep;
            if (isNeumann())
            {
                int currentiIteration = 0;
                for (double currentTimeStep = 0; currentTimeStep < processFullTime; currentTimeStep += timeStep)
                {
                    if (this.velocity != 0)
                    {
                        changeTempUsingVelocity(currentTimeStep, this.velocity);
                    }

                    c.Add(countDiffusionInTimeStep(D, currentiIteration));
                    this.integrals.Add(countIntegralFromRow(c.Last<double[]>()));
                    double dksi = this.integrals[currentiIteration] / 0.98;
                    if (dksi - this.ksi >= dx)
                    {
                        this.ksi = (int)Math.Ceiling(dksi);
                    }
         
                    //Writing results to file
                    writeDataToFile(c[currentiIteration], this.ksi, currentTimeStep, currentiIteration, 1000, 100);
                    currentiIteration++;
                    drawOnAusteniteVolumeChart(currentiIteration, this.ksi);
                }
            }
            else
            {
                Console.WriteLine("Neuman nie spelniony!");
            }

            file.Close();
        }
        int serie = 0;
        private void writeDataToFile(double[] dataTable, int ksi, double currentTimeStep, int iteration, double writeStep, int maxCellCount)
        {
            if (iteration % writeStep == 0)
            {
                file.Write(ksi.ToString() + "\t");
                // file.Write(iteration.ToString() + "\t");
                file.Write(currentTimeStep.ToString("F0") + "\t");
                for (int i = 0; i < maxCellCount; i++)
                {
                    file.Write(dataTable[i].ToString() + "\t");
                }
                file.Write("\n");
                drawOnChart(dataTable, iteration, serie);
                serie++;
            }
        }

        void drawOnChart(double[] dataTable, int iteration, int serie)
        {
            chart.Series.Add(serie.ToString());
            chart.Series[serie.ToString()].IsVisibleInLegend = false;
            chart.ChartAreas[0].AxisX.IsMarginVisible = false;
            for (int i = 0; i < 100; i++)
            {
                chart.Series[serie].Points.AddXY(i, dataTable[i]);
                chart.Series[serie].ChartType = SeriesChartType.Line;
            }
        }

        void drawOnAusteniteVolumeChart(int iteration, int ksi)
        {
            chartAInT.Series[0].IsVisibleInLegend = false;
            chartAInT.ChartAreas[0].AxisX.IsMarginVisible = false;
            chartAInT.Series[0].Points.AddXY(iteration, ksi);
            chartAInT.Series[0].ChartType = SeriesChartType.Line;
        }
    }
}
